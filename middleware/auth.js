const jwt = require('jsonwebtoken');
const { Exception } = require('../utils/Excpetions');

function auth(req, res, next){
    const token  = req.header('x-auth-token');
    if(!token) throw new Exception(401, 'Access Denied');

    try {
        const decoded = jwt.verify(token, process.env.jwtsecretkey);
        req.user = decoded;
        next();
    } catch (error) {
        throw new Exception(400, 'Invalid token provided');
    }
}

module.exports = {
    auth
}