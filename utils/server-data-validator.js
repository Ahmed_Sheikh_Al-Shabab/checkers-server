const Joi = require('@hapi/joi');


const singupValidate = (data) => {
    const schema = Joi.object({
        username: Joi.string()
           .alphanum()
           .min(4)
           .max(20)
           .required(),
    
        password: Joi.string()
           .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
           .required(), 
        
        repeat_password: Joi.ref('password'),
    
        email: Joi.string() 
           .email({minDomainSegments: 2, 
            tlds: {allow: ['com', 'net', 'org']} 
        })
           .required(), 
    })
    return schema.validate(data);
}

const loginValidate = (data) =>{
    const schema = Joi.object({
        email: Joi.string() 
        .email({minDomainSegments: 2, 
         tlds: {allow: ['com', 'net', 'org']} 
     })
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(), 
    })

    return schema.validate(data);
}

const updateValidate = (data) =>{
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .min(4)
            .max(20)
            .required(),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(), 
    })
    return schema.validate(data);
}

module.exports = {
    singupValidate,
    loginValidate,
    updateValidate
}