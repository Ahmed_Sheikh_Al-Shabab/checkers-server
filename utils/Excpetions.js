const { stubString } = require("lodash");

class Exception extends Error{
    constructor(statusCode, message){
        super();
        this.statusCode = statusCode;
        this.message = message;
    }
}

const handleExecption = (err, res) =>{
    const {statusCode, message} = err;
    console.log(statusCode);
    res.status(statusCode).json({
        status: 'error',
        statusCode: statusCode || 500,
        message,
    })
}

module.exports = {
    Exception,
    handleExecption,
}