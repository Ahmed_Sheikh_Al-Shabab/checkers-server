const bcrypt = require('bcrypt');

function hashingPassword(password){
    const salt = bcrypt.genSaltSync(10);
    const hasedPassword = bcrypt.hashSync(password, salt);
    return hasedPassword;
}

function comparedPassword(password, encryptedPassword) {
    const result = bcrypt.compareSync(password, encryptedPassword);
    return result;
}

module.exports ={
    hashingPassword,
    comparedPassword
}