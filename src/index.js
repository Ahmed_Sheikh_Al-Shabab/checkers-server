const result = require('dotenv').config();
const express = require('express');
const socketio = require('socket.io');
const uniqid = require('uniqid');
const http = require('http');
const {validMove} = require('../utils/validation');
const {findUser, addUser} = require('../model/socket_user');
const users = require('../routes/users');
const auth = require('../routes/auth');
const erroHandler = require('../middleware/error');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

if(result.error){
    console.log('FATAL ERROR');
    process.exit(1);
}

const port = process.env.PORT || 3000;

app.use(express.json());
app.use('/api/users', users);
app.use('/api/login', auth);

app.use(erroHandler);

let roomIDS = [];

io.on('connection', (socket) => {

    console.log('new Java client')

    socket.on('sendmove', (move, id, callback) => {
        const user = findUser(id);
        console.log(user);
        if(!validMove(move)){
            //validation
            console.log("illegalMove")
            return callback({Accepted: false});
        } 
        // console.log('Move', move);
        socket.broadcast.to(user.roomID).emit('move', move);
    })

    socket.on('create room', (callback) => {
        const roomID = uniqid(); 
        roomIDS.push(roomID);
        addUser(socket.id, roomID);
        socket.join(roomID);
        console.log('New room has been created with id: ' + roomID);    //     callback(id);
        callback(roomID);
    })

    socket.on('join room', (roomID) =>{
        addUser(socket.id, roomID);
        if(roomIDS.includes(roomID)){
            socket.join(roomID);
            console.log('new Player has been joined to the room' + roomID);
            io.to(roomID).emit('game started');
        }
    })
    
})

server.listen(port, () => {
    console.log('server is up on port: ' + port);
})

