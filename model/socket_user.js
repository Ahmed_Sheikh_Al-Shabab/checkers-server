let userCollection = [];

const addUser = (id, roomID, name='', password='', email='') =>{
    const user = {
        id,
        name,
        password,
        email,
        roomID
    }
    userCollection.push(user);
}

const findUser = (id) =>{
    let foundedUser = userCollection.find((obj) => {
        if(obj.id === id) return obj;
    })
    return foundedUser;
}

module.exports = {
    addUser,
    findUser
}
