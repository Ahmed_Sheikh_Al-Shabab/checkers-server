const {v1: uuidv1} = require('uuid');
const fs = require('fs');
const {Exception} = require('../utils/Excpetions');



const addUser = (name, email, password) =>{
    let users = loadUsers();
    const user = {
        id: uuidv1(),
        name,
        email,
        password,
        score: 0,
    }
    users.push(user);
    fs.writeFileSync('./model/user.json', JSON.stringify(users));
    return user;
}

const loadUsers = () =>{
    const data = fs.readFileSync('./model/user.json', 'utf-8');
    parsedData = JSON.parse(data);
    return parsedData;
}

const findUser = (id) =>{
    const users = loadUsers();
    const user = users.find((u) => u.id === id);
    if(user) return user;
    throw new Exception(404 ,`user with id ${id} was not found`);
}

const findByEmail = (obj) =>{
    const users = loadUsers();
    const user = users.find((user) => user.email === obj.email);
    return user;
}

const updateUser = (id, name, password) =>{
    let users = loadUsers();
    try {
        findUser(id)
        const index = users.map((u) => u.id).indexOf(id);
        users[index].name = name;
        users[index].password = password;
        fs.writeFileSync('./model/user.json', JSON.stringify(users));
        return users[index];
    } catch (e) {
        throw e;
    }
}

const updateScoer = (id, score) =>{
    let users = loadUsers();
    try {
        findUser(id)
        const index = users.map((u) => u.id).indexOf(id);
        users[index].score = score;
        fs.writeFileSync('./model/user.json', JSON.stringify(users));
        return users[index];
    } catch (e) {
        throw e;
    }
}

const deleteUser = (id) =>{
    let users = loadUsers();
    try{
        findUser(id);
        const index = users.map((u) => u.id).indexOf(id);
        const deletedUser = users[index];
        users.splice(index, 1);
        fs.writeFileSync('./model/user.json', JSON.stringify(users));
        return deletedUser;
    }catch(e){
        throw e;
    }
}

module.exports = {
    addUser,
    findUser,
    loadUsers,
    updateUser,
    updateScoer,
    deleteUser,
    findByEmail,
}
