const express = require('express');
const _ = require('lodash');
const jwt = require('jsonwebtoken');

const {auth} = require('../middleware/auth');
const {findUser, loadUsers, addUser, updateUser, deleteUser, findByEmail, updateScoer} = require('../model/user');
const {hashingPassword} = require('../utils/Hash');
const {singupValidate, updateValidate} = require('../utils/server-data-validator');

const {Exception} = require('../utils/Excpetions');
const user = require('../model/user');

const router = express.Router();

router.post('/', (req, res, next) =>{
    try {
        const {error} = singupValidate(req.body);
        if(error) throw new Exception(400, error.details[0].message);
        
        const usere = findByEmail({email: req.body.email});
        if(usere) throw new Exception(400, 'Already have an account try to login');
        
        req.body.password = hashingPassword(req.body.password);
        const user = addUser(req.body.username, req.body.email, req.body.password);
        
        const token = jwt.sign({_id: user.id}, process.env.jwtsecretkey);
        user.token = token;
        return res.header('x-auth-token', token).status(200).send(user);
    } catch (error) {
        next(error);
    }
})

router.get('/', (req, res) => {
    const users = loadUsers();
    return res.status(200).send(users);
}); 

router.get('/leader', (req, res) => {
    const users = loadUsers();
    users.sort((a, b) => b.score - a.score);
    const leadres = [];
    users.forEach(element => {
        const res = _.pick(element, ['name', 'score']);
        leadres.push(res);
    });
    return res.status(200).send({leadres});
})

router.put('/updateScore', auth, (req, res) => {
    try {
        if(!req.body.score) res.status(400).send('no score provided');
        const user = updateScoer(req.user._id, req.body.score);
        return res.status(200).send({score: user.score});
    } catch (err) {
        next(err);
    }
}) 

router.get('/me', auth, (req, res) =>{
    try {
        const user = findUser(req.user._id);
        res.status(200).send(_.pick(user, ['name', 'email', 'score']));
    } catch (e) {
        return res.status(404).json({
            status: 'error',
            message: e.message
        })
    }
})

router.put('/update', auth, (req, res) => {
    try{
        const {error} = updateValidate(req.body);
        if(error) throw new Exception(400, error.details[0].message);
        const user = updateUser(req.user._id, req.body.username, hashingPassword(req.body.password));
        return res.status(200).send(user);
    } catch (e) {
        return res.status(404).json({
            status: 'error',
            message: e.message
        })
    }
})

router.delete('/:id', (req, res) => {
    try {
        const user = deleteUser(req.params.id);
        return res.status(200).send(user);
    } catch (e) {
        res.status(404).json({
            status: 'error',
            message: e.message + e.statusCode
        })
        return;
    }
})

module.exports = router;