const express = require('express');
const jwt = require('jsonwebtoken');
const {Exception} = require('../utils/Excpetions');
const {loginValidate} = require('../utils/server-data-validator');
const {comparedPassword} = require('../utils/Hash');
const {findByEmail} = require('../model/user');

const router = express.Router()

router.post('/', (req, res) =>{
    const {error} = loginValidate(req.body);
    if(error) throw new Exception(400, error.details[0].message);

    const user = findByEmail({email: req.body.email});
    if(!user) throw new Exception(400, 'Invalide email or password');

    const result = comparedPassword(req.body.password, user.password);
    if(!result) throw new Exception(400, 'Invalide email or password');

    const token = jwt.sign({id: user.id}, process.env.jwtsecretkey);

    res.header('x-auth-token', token).status(200).send({name: user.name, score: user.score, token});

})

module.exports = router;